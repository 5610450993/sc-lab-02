package view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InvestmentFrame extends JFrame
{
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;

	private static final double DEFAULT_RATE = 5;
	
	private JLabel rateLabel;
	private JTextField rateField;
	private JButton button;
	private JLabel resultLabel;
	private JPanel panel;
	
	public InvestmentFrame(double b)
	{
	    setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		
	    resultLabel = new JLabel("balance: " + b);
	    rateLabel = new JLabel("Interest Rate: ");
	    
	    final int FIELD_WIDTH = 10;
	    rateField = new JTextField(FIELD_WIDTH);
	    rateField.setText("" + DEFAULT_RATE);	
	    
	    button = new JButton("Add Interest");
	    
	    panel = new JPanel();
	    panel.add(rateLabel);
	    panel.add(rateField);
	    panel.add(button);
	    panel.add(resultLabel);      
	    add(panel);
	    
	    setVisible(true);
	}
	
	
	public JTextField getRateField()
	{
		return rateField;
	}
	
	public JLabel getResultLabel()
	{
		return resultLabel;
	}
	
	public JButton getButton()
	{
		return button;
	}
}
