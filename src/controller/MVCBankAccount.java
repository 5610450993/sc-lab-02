package controller;

import view.InvestmentFrame;
import model.BankAccount;

public class MVCBankAccount {
	public static void main(String[] args)
	{	BankAccount m = new BankAccount(1000);
		InvestmentFrame v = new InvestmentFrame(m.getBalance());
		BankAccountController c = new BankAccountController(m, v);
		c.BankAccountListener();
	}
}
