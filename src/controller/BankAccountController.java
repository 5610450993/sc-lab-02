package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.BankAccount;
import view.InvestmentFrame;
//controller �� object ����˹�ҷ���Ѻ�����������¡�� object BankAccount ��� InvestmentFrame ���ӧҹ�����ѹ
public class BankAccountController {
	private BankAccount model;
	private InvestmentFrame view;
	private ActionListener actionListener;
	
	public BankAccountController(BankAccount model, InvestmentFrame view)
	{
		this.model = model;
		this.view = view;
		
	}
	
	public void BankAccountListener()
	{
		actionListener = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {            
				double rate = Double.parseDouble(view.getRateField().getText());
	            double interest = model.getBalance() * rate / 100;
	            model.deposit(interest);
	            view.getResultLabel().setText("balance: " + model.getBalance());
            }
      };   
      view.getButton().addActionListener(actionListener); 
	}
}
